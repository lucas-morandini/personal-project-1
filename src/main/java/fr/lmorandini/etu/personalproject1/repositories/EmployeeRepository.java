package fr.lmorandini.etu.personalproject1.repositories;

import fr.lmorandini.etu.personalproject1.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
